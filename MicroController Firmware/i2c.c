#include "i2c.h"

/** Initialize i2c **/
void i2c_init(void)
{

	i2c_handle.Instance = I2Cx;
	i2c_handle.Init.ClockSpeed = 400000;
	i2c_handle.Init.DutyCycle = I2C_DUTYCYCLE_2;
	i2c_handle.Init.OwnAddress1 = 0;
	i2c_handle.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	i2c_handle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLED;
	i2c_handle.Init.OwnAddress2 = 0;
	i2c_handle.Init.GeneralCallMode = I2C_GENERALCALL_DISABLED;
	i2c_handle.Init.NoStretchMode = I2C_NOSTRETCH_DISABLED;

	HAL_I2C_Init(&i2c_handle);
}

/** Write a byte to a VL6180X **/
void i2c_range_write_byte(uint16_t slave_addr, uint16_t reg_addr, uint8_t data)
{
	int size = 3;
	i2c_transmit_buff[0] = reg_addr >> 8;
	i2c_transmit_buff[1] = reg_addr & 0x00FF;
	i2c_transmit_buff[2] = data;

	while (HAL_I2C_Master_Transmit(&i2c_handle, slave_addr | I2C_WRITE, (uint8_t*) i2c_transmit_buff, size, 10000) != HAL_OK)
	{
		
		if (HAL_I2C_GetError(&i2c_handle) != HAL_I2C_ERROR_AF)
		{
			i2c_error_handler();
		}
	}
}

/** Write a byte to the OV7690 **/
void i2c_cam_write_byte(uint16_t slave_addr, uint8_t reg_addr, uint8_t data)
{
	int size = 2;
	i2c_transmit_buff[0] = reg_addr & 0x00FF;
	i2c_transmit_buff[1] = data;

	while (HAL_I2C_Master_Transmit(&i2c_handle, slave_addr | I2C_WRITE, (uint8_t*) i2c_transmit_buff, size, 10000) != HAL_OK)
	{
		
		if (HAL_I2C_GetError(&i2c_handle) != HAL_I2C_ERROR_AF)
		{
			i2c_error_handler();
		}
	}
}

/** Write size bytes in the transmit buff**/
void i2c_write_buff(uint16_t slave_addr, uint8_t size)
{
	while (HAL_I2C_Master_Transmit(&i2c_handle, slave_addr | I2C_WRITE, (uint8_t*) i2c_transmit_buff, size, 10000) != HAL_OK)
	{
		if (HAL_I2C_GetError(&i2c_handle) != HAL_I2C_ERROR_AF)
		{
			
			i2c_error_handler();
		}
	}
}

/** Receive size bytes**/
void i2c_read_buff(uint16_t slave_addr, uint8_t size)
{
	while (HAL_I2C_Master_Receive(&i2c_handle, slave_addr | I2C_READ, (uint8_t *)i2c_receive_buff, size, 10000) != HAL_OK)
	{

		if (HAL_I2C_GetError(&i2c_handle) != HAL_I2C_ERROR_AF)
		{
			i2c_error_handler();
		}   
	}
}

/** Error handler **/
void i2c_error_handler(void)
{
	/* Turn LED on */
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_14, GPIO_PIN_SET);
	while (1)
	{
	}
}