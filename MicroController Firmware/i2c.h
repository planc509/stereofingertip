#ifndef __I2C_H
#define __I2C_H

#include <stm32f4xx_hal.h>

/* Definition for I2Cx clock resources */
#define I2Cx                             I2C2
#define I2Cx_CLK_ENABLE()                __HAL_RCC_I2C2_CLK_ENABLE()
#define I2Cx_SDA_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOC_CLK_ENABLE()
#define I2Cx_SCL_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE() 

#define I2Cx_FORCE_RESET()               __HAL_RCC_I2C2_FORCE_RESET()
#define I2Cx_RELEASE_RESET()             __HAL_RCC_I2C2_RELEASE_RESET()

/* Definition for I2Cx Pins */
#define I2Cx_SCL_PIN                    GPIO_PIN_10
#define I2Cx_SCL_GPIO_PORT              GPIOB
#define I2Cx_SCL_AF                     GPIO_AF4_I2C2
#define I2Cx_SDA_PIN                    GPIO_PIN_12
#define I2Cx_SDA_GPIO_PORT              GPIOC
#define I2Cx_SDA_AF                     GPIO_AF4_I2C2

#define I2C_WRITE						0
#define I2C_READ						1

#define I2C_BUFFER_SIZE 10

I2C_HandleTypeDef i2c_handle;

uint8_t i2c_transmit_buff[I2C_BUFFER_SIZE];
uint8_t i2c_receive_buff[I2C_BUFFER_SIZE];

/** Initialize i2c **/
void i2c_init(void);

/** Write a byte to a VL6180X **/
void i2c_range_write_byte(uint16_t slave_addr, uint16_t reg_addr, uint8_t data);

/** Write a byte to the OV7690 **/
void i2c_cam_write_byte(uint16_t slave_addr, uint8_t reg_addr, uint8_t data);

/** Write size bytes in the transmit buff**/
void i2c_write_buff(uint16_t slave_addr, uint8_t size);

/** Receive size bytes**/
void i2c_read_buff(uint16_t slave_addr, uint8_t size);

/** Error handler **/
void i2c_error_handler(void);

#endif /* __I2C_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/