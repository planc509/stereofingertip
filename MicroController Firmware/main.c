#include <stm32f4xx_hal.h>
#include <stdlib.h>
#include "ov7690.h"
#include "stm32f446_cam.h"
#include "spi.h"
#include "vl6180x.h"

#define IMAGE_ROWS 210
#define IMAGE_COLS 280

static void SystemClock_Config(void);
uint8_t frame_buff[2*IMAGE_ROWS * IMAGE_COLS];

typedef enum { SPI_WAIT, FRAME_END_WAIT, BEGIN_TRANSMISSION, TRANSMITTING, HALTED } cam_status;
cam_status cam_stat1 = SPI_WAIT;
extern DCMI_HandleTypeDef  hDcmiEval;
extern OV7690* cam_devices[1];
extern VL6180X* range_devices[2];
int main(void)
{
	HAL_Init();
	SystemClock_Config();
	i2c_init();
	

	__GPIOC_CLK_ENABLE();
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.Pin = GPIO_PIN_14;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_14, GPIO_PIN_RESET);

	__GPIOA_CLK_ENABLE();
	GPIO_InitStructure.Pin = GPIO_PIN_9 | GPIO_PIN_10;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);
	HAL_Delay(1);

	OV7690 front_tip_cam;
	front_tip_cam.addr = DEFAULT_CAM_SLAVE_ADDR;
	cam_devices[0] = &front_tip_cam;

	VL6180X front_tip_range;
	uint8_t new_addr = fix_address(&front_tip_range, 0x20);
	change_address(&front_tip_range, 0x20);
	range_devices[0] = &front_tip_range;
	setup_range(&front_tip_range, 0x20, NULL, GPIO_PIN_0);
//	start_range(&front_tip_range);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);
	HAL_Delay(1); 
	VL6180X pad_range;
	new_addr = fix_address(&pad_range, RANGE_DEFAULT_SLAVE_ADDR);
	change_address(&pad_range, RANGE_DEFAULT_SLAVE_ADDR);
	range_devices[1] = &pad_range;
	setup_range(&pad_range, RANGE_DEFAULT_SLAVE_ADDR, GPIOA, GPIO_PIN_10);
//	start_range(&pad_range);

	__GPIOB_CLK_ENABLE();
	GPIO_InitStructure.Pin = GPIO_PIN_12;
	GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 11, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);


	
	BSP_CAMERA_StartClock();
	HAL_Delay(1);
	

	setup_cam_yuv(&front_tip_cam, DEFAULT_CAM_SLAVE_ADDR, GPIOA, GPIO_PIN_9, IMAGE_ROWS, IMAGE_COLS);	

	BSP_CAMERA_Init(IMAGE_ROWS, IMAGE_COLS);
	BSP_CAMERA_ContinuousStart(frame_buff); 

	uint32_t buff_length = 2*IMAGE_ROWS * IMAGE_COLS;
	uint32_t inc = 508;
	uint8_t range_countdown = 20;
	uint8_t read_front = 0;
	for (;;)
	{
		spi_init(); // Put this here to restart spi, necessary?
		while (cam_stat1 != BEGIN_TRANSMISSION)
			;
		
		cam_stat1 = TRANSMITTING;

	//	HAL_SPI_Transmit(&SpiHandle, frame_buff, buff_length, 5000);
		uint32_t img_count = 0;
		
		uint8_t offset = 0;
		uint32_t transmit = buff_length - img_count + 2 > inc ? inc : buff_length - img_count + 2;
//		update_range(&pad_range);
//		update_range(&front_tip_range);	

		spi_tx_buff[offset*inc] = front_tip_range.distance;
		spi_tx_buff[offset*inc + 1] = pad_range.distance;
		for (int i = 2; i < transmit; i++)
		{
			spi_tx_buff[offset*inc + i] = frame_buff[(i-2) + img_count];
		}

		while (img_count < buff_length)
		{
			
			HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_14);
			HAL_SPI_Transmit_DMA(&SpiHandle, (uint8_t*)(spi_tx_buff+offset*inc), transmit);
			img_count += transmit-2;
	
			if (range_countdown <= 0)
			{
				if (read_front == 1)
				{
					update_range(&front_tip_range);
					start_measurement(&pad_range);
					read_front = 0;
				}
				else
				{
					update_range(&pad_range);
					start_measurement(&front_tip_range);
					read_front = 1;
				}
				range_countdown = 12;	
			}
			else
			{
				range_countdown--;
			}
			

			offset = 1 - offset;
			transmit = buff_length - img_count + 2 > inc ? inc : buff_length - img_count + 2;
			
			spi_tx_buff[offset*inc] = front_tip_range.distance;
			spi_tx_buff[offset*inc + 1] = pad_range.distance;
			for (int i = 2; i < transmit; i++)
			{
				spi_tx_buff[offset*inc + i] = frame_buff[(i-2) + img_count];
			}


			while (HAL_SPI_GetState(&SpiHandle) != HAL_SPI_STATE_READY)
			{
			} 
			
		}
		
	//	spi_deinit();
		HAL_Delay(1);
		cam_stat1 = SPI_WAIT;
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == GPIO_PIN_12)
	{
		if (cam_stat1 == SPI_WAIT)
		{
	//		cam_stat1 = FRAME_END_WAIT;	
			cam_stat1 = BEGIN_TRANSMISSION;
		}
		
	}
	
}

void BSP_CAMERA_FrameEventCallback(void)
{
	
	
	if (cam_stat1 == FRAME_END_WAIT)
	{
		cam_stat1 = BEGIN_TRANSMISSION;
	}
	else if (cam_stat1 == TRANSMITTING)
	{
		
	//	BSP_CAMERA_Suspend();
	//	__HAL_DMA_DISABLE(hDcmiEval.DMA_Handle);
		cam_stat1 = HALTED;
//		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_14, GPIO_PIN_RESET);
	}
//	BSP_CAMERA_Resume();

//	HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_14);
//	BSP_CAMERA_SnapshotStart(frame_buff); 
  /* Display on LCD */
//	BSP_LCD_DrawRGBImage(0, 0, 320, 240, (uint8_t *)CAMERA_FRAME_BUFFER);
}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	HAL_StatusTypeDef ret = HAL_OK;
	__PWR_CLK_ENABLE();

	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 16;
	RCC_OscInitStruct.PLL.PLLN = 360;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 2;
	RCC_OscInitStruct.PLL.PLLR = 2;
	ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
	if (ret != HAL_OK)
	{
		while (1)
			;
	}

	ret = HAL_PWREx_ActivateOverDrive();
	if (ret != HAL_OK)
	{
		while (1)
			;
	}

	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
	                            | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
	ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5);
	if (ret != HAL_OK)
	{
		while (1)
			;
	}
//	HAL_RCC_MCOConfig(RCC_MCO1, RCC_MCO1SOURCE_HSI, RCC_MCODIV_1);

//	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

//	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	  /* SysTick_IRQn interrupt configuration */
//	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}
