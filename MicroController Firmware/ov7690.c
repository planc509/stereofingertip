#include "ov7690.h"

/** Initialize the camera **/
void setup_cam_yuv(OV7690* cam_ptr, uint8_t addr, GPIO_TypeDef* port, uint16_t pin, uint16_t n_rows, uint16_t n_cols)
{

	cam_ptr->addr = addr;
	cam_ptr->port = port;
	cam_ptr->pin = pin;

	i2c_cam_write_byte(cam_ptr->addr, CAM_REG12, 0x80);	// System reset
	HAL_Delay(1);
	i2c_cam_write_byte(cam_ptr->addr, CAM_REG12, 0x00);	// System un-reset
	HAL_Delay(1);
	
	i2c_cam_write_byte(cam_ptr->addr, CAM_PWC0, 0x0D);	// DOVDD range selection (2.7V)
	i2c_cam_write_byte(cam_ptr->addr, CAM_REG0C, 0x06); // Configure data, PCLK, HREF, and VSYNC as outputs
	i2c_cam_write_byte(cam_ptr->addr, CAM_REG3E, 0x30);	// Set PCLK of YUV output double the PCLK of raw output
	i2c_cam_write_byte(cam_ptr->addr, CAM_CLKRC, 0x00);
	i2c_cam_write_byte(cam_ptr->addr, CAM_PLL, 0x02);	// Bypass PLL

	i2c_cam_write_byte(cam_ptr->addr, CAM_GAIN, 0xFF); // 4x Analog Gain
	i2c_cam_write_byte(cam_ptr->addr, CAM_REG15, 0x03);	// 4x Digit Gain

/*	i2c_cam_write_byte(cam_ptr->addr, CAM_REGC8, 0x02);	// Input size
	i2c_cam_write_byte(cam_ptr->addr, CAM_REGC9, 0x0D);	
	i2c_cam_write_byte(cam_ptr->addr, CAM_REGCA, 0x01);
	i2c_cam_write_byte(cam_ptr->addr, CAM_REGCB, 0xB0); */

	i2c_cam_write_byte(cam_ptr->addr, CAM_REGCC, n_cols >> 8);	// Output size
	i2c_cam_write_byte(cam_ptr->addr, CAM_REGCD, n_cols & 0xFF);
	i2c_cam_write_byte(cam_ptr->addr, CAM_REGCE, n_rows >> 8);
	i2c_cam_write_byte(cam_ptr->addr, CAM_REGCF, n_rows & 0xFF);
}