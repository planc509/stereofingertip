#ifndef __OV7690_H
#define __OV7690_H

#include <stm32f4xx_hal.h>
#include "i2c.h"

#define DEFAULT_CAM_SLAVE_ADDR 0x42

#define CAM_GAIN    0x00	// General gain control
#define CAM_BGAIN   0x01	// Blue gain control
#define CAM_RGAIN   0x02	// Red gain control
#define CAM_GGAIN   0x03	// Green gain control

#define CAM_PWC0	0x49	// Digital voltage range config
#define CAM_REG3E	0x3E	// PCLK COnfig
#define CAM_PLL		0x29	// PLL Config
#define CAM_REG12	0x12	// Reset and data format
#define CAM_CLKRC	0x11	// Clock config
#define CAM_REG0C	0x0C	// IO config
#define CAM_REG0E	0x0E	// Output data selection
#define CAM_REG15	0x15	// Frame control
#define CAM_REG16	0x16	// Windowing config
#define CAM_HSTART	0x17	// Horizontal window start
#define CAM_HSIZE	0x18	// Horizontal window size
#define CAM_VSTART	0x19	// Vertical window start
#define CAM_VSIZE	0x1A	// Vertical window size
#define CAM_REG28	0x28	// HREF and VSYN alt config
#define CAM_REG81	0x81	// Special Effects and Scaling
#define CAM_REG3F	0x3F	// PCLK polarity

#define CAM_REGB4	0xB4	// Sharpenning - Denoising auto/manual mode selection.
#define CAM_REGB6	0xB6	// Sharpenning magnitude adjustment.

#define CAM_REGC8	0xC8	// Horizontal input size MSBs
#define CAM_REGC9	0xC9	// Horizontal input size LSBs
#define CAM_REGCA	0xCA	// Vertical input size MSB
#define CAM_REGCB	0xCB	// Vertical input size LSBs

#define CAM_REGCC	0xCC	// Horizontal output size MSBs
#define CAM_REGCD	0xCD	// Horizontal output size LSBs
#define CAM_REGCE	0xCE	// Vertical output size MSB
#define CAM_REGCF	0xCF	// Vertical output size LSBs

typedef struct
{
	uint8_t addr; // The slave address of the camera
	GPIO_TypeDef* port; // The port that controls PWDN on the camera
	uint16_t pin; // The pint that controls PWDN on the camera
} OV7690;

#define N_CAM_DEVICES 1
OV7690* cam_devices[N_CAM_DEVICES];

/** Initialize the camera**/
void setup_cam_yuv(OV7690* cam_ptr, uint8_t addr, GPIO_TypeDef* port, uint16_t pin, uint16_t n_rows, uint16_t n_cols);

#endif