#include "spi.h"

/** Initialize spi **/
void spi_init(void) {
	SpiHandle.Instance = SPIx;
	SpiHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
	SpiHandle.Init.Direction = SPI_DIRECTION_2LINES;
	SpiHandle.Init.CLKPhase = SPI_PHASE_1EDGE;
	SpiHandle.Init.CLKPolarity = SPI_POLARITY_LOW;
	SpiHandle.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	SpiHandle.Init.CRCPolynomial = 7;
	SpiHandle.Init.DataSize = SPI_DATASIZE_8BIT;
	SpiHandle.Init.FirstBit = SPI_FIRSTBIT_MSB;
	SpiHandle.Init.NSS = SPI_NSS_SOFT;
	SpiHandle.Init.TIMode = SPI_TIMODE_DISABLE;
	SpiHandle.Init.Mode = SPI_MODE_SLAVE;

	if (HAL_SPI_Init(&SpiHandle) != HAL_OK)
	{
		HAL_SPI_ErrorCallback(&SpiHandle);
	}

}

void spi_deinit(void)
{
	HAL_SPI_DeInit(&SpiHandle);
}

void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi) {
	while (1)
		;

}

