#include "stm32f446_cam.h"

DCMI_HandleTypeDef  hDcmiEval;
uint32_t currentResolution;

static uint32_t GetSize(uint32_t Resolution);

/** Setup DCMI **/
uint8_t BSP_CAMERA_Init(uint32_t n_rows, uint32_t n_cols)
{ 
	DCMI_HandleTypeDef *phdcmi;
  
	uint8_t ret = CAMERA_ERROR;
  
	/* Get the DCMI handle structure */
	phdcmi = &hDcmiEval;

	  /*** Configures the DCMI to interface with the camera module ***/
	  /* DCMI configuration */
	phdcmi->Init.CaptureRate      = DCMI_CR_ALL_FRAME;
	phdcmi->Init.HSPolarity       = DCMI_HSPOLARITY_LOW;
	phdcmi->Init.SynchroMode      = DCMI_SYNCHRO_HARDWARE;
	phdcmi->Init.VSPolarity       = DCMI_VSPOLARITY_HIGH;
	phdcmi->Init.ExtendedDataMode = DCMI_EXTEND_DATA_8B;
	phdcmi->Init.PCKPolarity      = DCMI_PCKPOLARITY_RISING;
	phdcmi->Init.ByteSelectMode   = DCMI_BSM_ALL;
	phdcmi->Init.ByteSelectStart  = DCMI_OEBS_ODD;
	phdcmi->Init.LineSelectMode   = DCMI_LSM_ALL;
	phdcmi->Init.LineSelectStart  = DCMI_OELS_ODD; 
	phdcmi->Instance              = DCMI;

	/* DCMI Initialization */
	BSP_CAMERA_MspInit(&hDcmiEval, NULL);
	HAL_DCMI_Init(phdcmi);

	currentResolution = n_rows * n_cols;
	ret = CAMERA_OK;
  
	return ret;
}


/** Deinitialize DCMI **/
uint8_t BSP_CAMERA_DeInit(void)
{ 
	hDcmiEval.Instance              = DCMI;

	HAL_DCMI_DeInit(&hDcmiEval);
	BSP_CAMERA_MspDeInit(&hDcmiEval, NULL);
	return CAMERA_OK;
}

/**
  * @brief  Starts the camera capture in continuous mode.
  * @param  buff: pointer to the camera output buffer
  */
void BSP_CAMERA_ContinuousStart(uint8_t *buff)
{ 
  /* Start the camera capture */
	HAL_DCMI_Start_DMA(&hDcmiEval, DCMI_MODE_CONTINUOUS, (uint32_t)buff, GetSize(currentResolution));  
}

/**
  * @brief  Starts the camera capture in snapshot mode.
  * @param  buff: pointer to the camera output buffer
  */
void BSP_CAMERA_SnapshotStart(uint8_t *buff)
{ 
  /* Start the camera capture */
  HAL_DCMI_Start_DMA(&hDcmiEval, DCMI_MODE_SNAPSHOT, (uint32_t)buff, GetSize(currentResolution));  
}

/**
  * @brief Suspend the CAMERA capture 
  */
void BSP_CAMERA_Suspend(void) 
{
  /* Disable the DMA */
	__HAL_DMA_DISABLE(hDcmiEval.DMA_Handle);
	/* Disable the DCMI */
	__HAL_DCMI_DISABLE(&hDcmiEval);
  
}

/**
  * @brief Resume the CAMERA capture 
  */
void BSP_CAMERA_Resume(void) 
{
  /* Enable the DCMI */
	__HAL_DCMI_ENABLE(&hDcmiEval);
	/* Enable the DMA */
	__HAL_DMA_ENABLE(hDcmiEval.DMA_Handle);
}

/**
  * @brief  Stop the CAMERA capture 
  * @retval Camera status
  */
uint8_t BSP_CAMERA_Stop(void) 
{
	DCMI_HandleTypeDef *phdcmi;
  
	uint8_t ret = CAMERA_ERROR;
  
	/* Get the DCMI handle structure */
	phdcmi = &hDcmiEval;  
  
	if (HAL_DCMI_Stop(phdcmi) == HAL_OK)
	{
		ret = CAMERA_OK;
	}

	return ret;
}

void BSP_CAMERA_StartClock(void) 
{
	HAL_RCC_MCOConfig(RCC_MCO1, RCC_MCO1SOURCE_HSI, RCC_MCODIV_1);
}

void BSP_CAMERA_StopClock(void)
{
	HAL_RCC_MCOConfig(RCC_MCO1, RCC_MCO1SOURCE_HSE, RCC_MCODIV_1);
}

/**
  * @brief  Handles DCMI interrupt request.
  */
void BSP_CAMERA_IRQHandler(void) 
{
	HAL_DCMI_IRQHandler(&hDcmiEval);
}

/**
  * @brief  Handles DMA interrupt request.
  */
void BSP_CAMERA_DMA_IRQHandler(void) 
{
	HAL_DMA_IRQHandler(hDcmiEval.DMA_Handle);
}

/**
  * @brief  Get the capture size.
  * @param  Resolution: the current resolution.
  * @retval capture size.
  */
static uint32_t GetSize(uint32_t Resolution)
{ 
	uint32_t size = 2*Resolution / 4;
  
	return size;
}

/**
  * @brief  Initializes the DCMI MSP.
  * @param  hdcmi: HDMI handle 
  * @param  Params: pointer on additional configuration parameters, can be NULL.
  */
__weak void BSP_CAMERA_MspInit(DCMI_HandleTypeDef *hdcmi, void *Params)
{
	static DMA_HandleTypeDef hdma_eval;
	GPIO_InitTypeDef gpio_init_structure;
  
	/*** Enable peripherals and GPIO clocks ***/
	/* Enable DCMI clock */
	__HAL_RCC_DCMI_CLK_ENABLE();

	  /* Enable DMA2 clock */
	__HAL_RCC_DMA2_CLK_ENABLE(); 
  
	/* Enable GPIO clocks */
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOE_CLK_ENABLE();

	  /*** Configure the GPIO ***/
	  /* Configure DCMI GPIO as alternate function */
	gpio_init_structure.Pin       = GPIO_PIN_4 | GPIO_PIN_6; 
	gpio_init_structure.Mode      = GPIO_MODE_AF_PP;
	gpio_init_structure.Pull      = GPIO_PULLUP;
	gpio_init_structure.Speed     = GPIO_SPEED_HIGH;
	gpio_init_structure.Alternate = GPIO_AF13_DCMI;  
	HAL_GPIO_Init(GPIOA, &gpio_init_structure);

	gpio_init_structure.Pin       = GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9; 
	gpio_init_structure.Mode      = GPIO_MODE_AF_PP;
	gpio_init_structure.Pull      = GPIO_PULLUP;
	gpio_init_structure.Speed     = GPIO_SPEED_HIGH;
	gpio_init_structure.Alternate = GPIO_AF13_DCMI;   
	HAL_GPIO_Init(GPIOB, &gpio_init_structure);

	gpio_init_structure.Pin       = GPIO_PIN_6 | GPIO_PIN_7  | GPIO_PIN_8  |\
	                                GPIO_PIN_9 | GPIO_PIN_11; 
	gpio_init_structure.Mode      = GPIO_MODE_AF_PP;
	gpio_init_structure.Pull      = GPIO_PULLUP;
	gpio_init_structure.Speed     = GPIO_SPEED_HIGH;
	gpio_init_structure.Alternate = GPIO_AF13_DCMI;   
	HAL_GPIO_Init(GPIOC, &gpio_init_structure);
  
	  /*** Configure the DMA ***/
	  /* Set the parameters to be configured */
	hdma_eval.Init.Channel             = DMA_CHANNEL_1;
	hdma_eval.Init.Direction           = DMA_PERIPH_TO_MEMORY;
	hdma_eval.Init.PeriphInc           = DMA_PINC_DISABLE;
	hdma_eval.Init.MemInc              = DMA_MINC_ENABLE;
	hdma_eval.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
	hdma_eval.Init.MemDataAlignment    = DMA_MDATAALIGN_WORD;
	hdma_eval.Init.Mode                = DMA_CIRCULAR;
	hdma_eval.Init.Priority            = DMA_PRIORITY_HIGH;
	hdma_eval.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;         
	hdma_eval.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
	hdma_eval.Init.MemBurst            = DMA_MBURST_SINGLE;
	hdma_eval.Init.PeriphBurst         = DMA_PBURST_SINGLE; 

	hdma_eval.Instance = DMA2_Stream1;

	  /* Associate the initialized DMA handle to the DCMI handle */
	__HAL_LINKDMA(hdcmi, DMA_Handle, hdma_eval);
  
	/*** Configure the NVIC for DCMI and DMA ***/
	/* NVIC configuration for DCMI transfer complete interrupt */
	HAL_NVIC_SetPriority(DCMI_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DCMI_IRQn);  
  
	/* NVIC configuration for DMA2D transfer complete interrupt */
	HAL_NVIC_SetPriority(DMA2_Stream1_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA2_Stream1_IRQn);
  
	/* Configure the DMA stream */
	HAL_DMA_Init(hdcmi->DMA_Handle);  
}


/**
  * @brief  DeInitializes the DCMI MSP.
  * @param  hdcmi: HDMI handle 
  * @param  Params: pointer on additional configuration parameters, can be NULL.
  */
__weak void BSP_CAMERA_MspDeInit(DCMI_HandleTypeDef *hdcmi, void *Params)
{
    /* Disable NVIC  for DCMI transfer complete interrupt */
	HAL_NVIC_DisableIRQ(DCMI_IRQn);  
  
    /* Disable NVIC for DMA2 transfer complete interrupt */
	HAL_NVIC_DisableIRQ(DMA2_Stream1_IRQn);
  
    /* Configure the DMA stream */
	HAL_DMA_DeInit(hdcmi->DMA_Handle);  

	    /* Disable DCMI clock */
	__HAL_RCC_DCMI_CLK_DISABLE();

	    /* GPIO pins clock and DMA clock can be shut down in the application 
	       by surcharging this __weak function */ 
}

/**
  * @brief  Line event callback
  * @param  hdcmi: pointer to the DCMI handle  
  */
void HAL_DCMI_LineEventCallback(DCMI_HandleTypeDef *hdcmi)
{        
	BSP_CAMERA_LineEventCallback();
}

/**
  * @brief  Line Event callback.
  */
__weak void BSP_CAMERA_LineEventCallback(void)
{
  /* NOTE : This function Should not be modified, when the callback is needed,
            the HAL_DCMI_LineEventCallback could be implemented in the user file
   */
}

/**
  * @brief  VSYNC event callback
  * @param  hdcmi: pointer to the DCMI handle  
  */
void HAL_DCMI_VsyncEventCallback(DCMI_HandleTypeDef *hdcmi)
{        
	BSP_CAMERA_VsyncEventCallback();
}

/**
  * @brief  VSYNC Event callback.
  */
__weak void BSP_CAMERA_VsyncEventCallback(void)
{
  /* NOTE : This function Should not be modified, when the callback is needed,
            the HAL_DCMI_VsyncEventCallback could be implemented in the user file
   */
}

/**
  * @brief  Frame event callback
  * @param  hdcmi: pointer to the DCMI handle  
  */
void HAL_DCMI_FrameEventCallback(DCMI_HandleTypeDef *hdcmi)
{        
	BSP_CAMERA_FrameEventCallback();
}

/**
  * @brief  Frame Event callback.
  */
__weak void BSP_CAMERA_FrameEventCallback(void)
{
  /* NOTE : This function Should not be modified, when the callback is needed,
            the HAL_DCMI_FrameEventCallback could be implemented in the user file
   */
}

/**
  * @brief  Error callback
  * @param  hdcmi: pointer to the DCMI handle  
  */
void HAL_DCMI_ErrorCallback(DCMI_HandleTypeDef *hdcmi)
{        
	BSP_CAMERA_ErrorCallback();
}

/**
  * @brief  Error callback.
  */
__weak void BSP_CAMERA_ErrorCallback(void)
{
  /* NOTE : This function Should not be modified, when the callback is needed,
            the HAL_DCMI_ErrorCallback could be implemented in the user file
   */
}