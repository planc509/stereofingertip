#ifndef __STM32446E_EVAL_CAMERA_H
#define __STM32446E_EVAL_CAMERA_H

#include <stm32f4xx_hal.h>
 
typedef enum 
{
	CAMERA_OK      = 0x00,
	CAMERA_ERROR   = 0x01,
	CAMERA_TIMEOUT = 0x02 
}Camera_StatusTypeDef;

/** Setup DCMI **/
uint8_t BSP_CAMERA_Init(uint32_t n_rows, uint32_t n_cols);  

/** Deinitialize DCMI **/
uint8_t BSP_CAMERA_DeInit(void);

/** Start DCMI in continuous mode **/
void BSP_CAMERA_ContinuousStart(uint8_t *buff);

/** Start DCMI in snapshot mode **/
void BSP_CAMERA_SnapshotStart(uint8_t *buff);

/** Suspend DCMI **/
void BSP_CAMERA_Suspend(void);

/** Resume DCMI **/
void BSP_CAMERA_Resume(void);

/** Resume DCMI **/
uint8_t BSP_CAMERA_Stop(void);

/* HREF callback **/
void BSP_CAMERA_LineEventCallback(void);

/** VSYNC callback **/
void BSP_CAMERA_VsyncEventCallback(void);

/** Frame callback**/
void BSP_CAMERA_FrameEventCallback(void);

/** Error callback **/
void BSP_CAMERA_ErrorCallback(void);

/** Start the clock to the camera **/
void BSP_CAMERA_StartClock(void);

/** Stop the clock to the camera **/
void BSP_CAMERA_StopClock(void);

/* To be called in DCMI_IRQHandler function */
void    BSP_CAMERA_IRQHandler(void);
/* To be called in DMA2_Stream1_IRQHandler function */
void    BSP_CAMERA_DMA_IRQHandler(void);
   
/* These functions can be modified in case the current settings (e.g. DMA stream)
   need to be changed for specific application needs */
void BSP_CAMERA_MspInit(DCMI_HandleTypeDef *hdcmi, void *Params);
void BSP_CAMERA_MspDeInit(DCMI_HandleTypeDef *hdcmi, void *Params);

#endif /* __STM32446E_EVAL_CAMERA_H */

