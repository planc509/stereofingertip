#ifndef __STM32F4xx_IT_H
#define __STM32F4xx_IT_H

#include "stm32f4xx_hal.h"
#include "stm32f446_cam.h"

void NMI_Handler(void);
void HardFault_Handler(void);
void MemManage_Handler(void);
void BusFault_Handler(void);
void UsageFault_Handler(void);
void SVC_Handler(void);
void DebugMon_Handler(void);
void PendSV_Handler(void);
void SysTick_Handler(void);
void DMA2_Stream1_IRQHandler(void);
void DCMI_IRQHandler(void);
void SPIx_IRQHandler(void);
void EXTI15_10_IRQHandler(void);
void DMA1_Stream4_IRQHandler(void);
void SPIx_DMA_RX_IRQHandler(void);

#endif
