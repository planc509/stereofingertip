#include "vl6180x.h"

extern I2C_HandleTypeDef i2c_handle;
extern uint8_t i2c_transmit_buff[I2C_BUFFER_SIZE];
extern uint8_t i2c_receive_buff[I2C_BUFFER_SIZE];

// Initialize sensor
void setup_range(VL6180X* vl6180x_ptr, uint8_t addr, GPIO_TypeDef* port, uint16_t pin)
{
//	vl6180x_ptr->addr = addr;
	vl6180x_ptr->distance = 0;
	vl6180x_ptr->port = port;
	vl6180x_ptr->pin = pin;

//	change_address(vl6180x_ptr, addr);

	// Mandatory : private registers
	i2c_range_write_byte(vl6180x_ptr->addr, 0x0207, 0x01);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x0208, 0x01);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x0096, 0x00);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x0097, 0xfd);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00e3, 0x00);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00e4, 0x04);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00e5, 0x02);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00e6, 0x01);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00e7, 0x03);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00f5, 0x02);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00d9, 0x05);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00db, 0xce);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00dc, 0x03);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00dd, 0xf8);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x009f, 0x00);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00a3, 0x3c);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00b7, 0x00);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00bb, 0x3c);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00b2, 0x09);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00ca, 0x09);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x0198, 0x01);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x01b0, 0x17);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x01ad, 0x00);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x00ff, 0x05);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x0100, 0x05);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x0199, 0x05);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x01a6, 0x1b);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x01ac, 0x3e);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x01a7, 0x1f);
	i2c_range_write_byte(vl6180x_ptr->addr, 0x0030, 0x00);
	
	//Recommended : Public registers - See data sheet for more detail
	i2c_range_write_byte(vl6180x_ptr->addr, 0x0011, 0x10); // Enables polling for �New Sample ready� when measurement completes
	i2c_range_write_byte(vl6180x_ptr->addr, 0x010a, 0x30); // Set the averaging sample period (compromise between lower noise and increased execution time)
	i2c_range_write_byte(vl6180x_ptr->addr, 0x003f, 0x46); // Sets the light and dark gain (upper nibble). Dark gain should not be changed.
	i2c_range_write_byte(vl6180x_ptr->addr, 0x0031, 0xFF); // sets the # of range measurements after which auto calibration of system is performed
//	i2c_range_write_byte(vl6180x_ptr->addr, 0x0040, 0x63); // Set ALS integration time to 100ms DocID026571 Rev 1 25/27
	i2c_range_write_byte(vl6180x_ptr->addr, 0x0040, 0x32); // Set ALS integration time to 50ms
	i2c_range_write_byte(vl6180x_ptr->addr, 0x002e, 0x01); // perform a single temperature calibration of the ranging sensor
//	i2c_range_write_byte(vl6180x_ptr->addr, 0x003e, 0x31); // Set default ALS inter-measurement period to 500ms
	i2c_range_write_byte(vl6180x_ptr->addr, 0x003e, 0x0A); // Set default ALS inter-measurement period to 100ms
	i2c_range_write_byte(vl6180x_ptr->addr, 0x0014, 0x24); // Configures interrupt on �New Sample Ready threshold event�
	i2c_range_write_byte(vl6180x_ptr->addr, 0x001b, 0x02); // Set default ranging inter-measurement period to 30ms
	i2c_range_write_byte(vl6180x_ptr->addr, 0x001c, 0x14); // Set Max Range convergence time to 20ms
	i2c_range_write_byte(vl6180x_ptr->addr, 0x003f, 0x47); // Set the ALS analog gain to 40
//	i2c_range_write_byte(vl6180x_ptr->addr, 0x003f, 0x06);
//	i2c_range_write_byte(vl6180x_ptr->addr, 0x0021,0x00);  // Set valid height measurement to 0?
//	i2c_range_write_byte(vl6180x_ptr->addr, 0x0025,0x00);
}

uint8_t fix_address(VL6180X* vl6180x_ptr, uint8_t start_addr)
{
	vl6180x_ptr->addr = start_addr - 2;
	i2c_transmit_buff[0] = 0x00;
	i2c_transmit_buff[1] = 0x00;

	do
	{
		vl6180x_ptr->addr += 2;
		while (1)
		{
			uint8_t addr_free = 1;
			for (int i = 0; i < N_CAM_DEVICES; i++)
			{
				if (cam_devices[i] != NULL && vl6180x_ptr->addr == cam_devices[i]->addr)
				{
					addr_free = 0;
				}
			}
			for (int i = 0; i < N_RANGE_DEVICES; i++)
			{
				if (range_devices[i] != NULL && vl6180x_ptr->addr == range_devices[i]->addr)
				{
					addr_free = 0;
				}
			}
			if (addr_free)
			{
				break;
			}
			else
			{
				vl6180x_ptr->addr += 2;
			}
		}
	} while (HAL_I2C_Master_Transmit(&i2c_handle, (uint16_t) vl6180x_ptr->addr | I2C_WRITE, (uint8_t*) i2c_transmit_buff, 2, 10000) != HAL_OK);

	return vl6180x_ptr->addr;
}

// Change the sensor's slave address
uint8_t change_address(VL6180X* vl6180x_ptr, uint8_t new_addr)
{
	// Try to contact new address
/*	vl6180x_ptr->addr = new_addr;
	i2c_transmit_buff[0] = 0x00;
	i2c_transmit_buff[1] = 0x00;
	
	while (HAL_I2C_Master_Transmit(&i2c_handle, (uint16_t) vl6180x_ptr->addr | I2C_WRITE, (uint8_t*) i2c_transmit_buff, 2, 10000) != HAL_OK)
	{
		if (vl6180x_ptr->addr != RANGE_DEFAULT_SLAVE_ADDR)
		{
			vl6180x_ptr->addr = RANGE_DEFAULT_SLAVE_ADDR;
		}
		else
		{
			vl6180x_error_handler();
		}
	}

	if (vl6180x_ptr->addr == new_addr)
	{
		// Sensor already has this address
		return vl6180x_ptr->addr;
	}*/

	i2c_range_write_byte(vl6180x_ptr->addr, 0x0212, new_addr >> 1);

	// Read the address back
	vl6180x_ptr->addr = new_addr;
	i2c_transmit_buff[0] = 0x02;
	i2c_transmit_buff[1] = 0x12;
	i2c_receive_buff[0] = 0;
	i2c_write_buff(vl6180x_ptr->addr, 2);
	i2c_read_buff(vl6180x_ptr->addr, 1);

	// Set the received address and return it
	vl6180x_ptr->addr = i2c_receive_buff[0] << 1;
	if (vl6180x_ptr->addr != new_addr)
	{
		vl6180x_error_handler();
	}
	return vl6180x_ptr->addr;
}

// Begin ranging
void start_range(VL6180X* vl6180x_ptr)
{
	i2c_transmit_buff[0] = 0x00;
	i2c_transmit_buff[1] = 0x16;
	i2c_receive_buff[0] = 0;
	i2c_write_buff(vl6180x_ptr->addr, 2);
	i2c_read_buff(vl6180x_ptr->addr, 1);
	if (i2c_receive_buff[0] == 1)
	{
		i2c_range_write_byte(vl6180x_ptr->addr, 0x0016, 0x00);
		i2c_range_write_byte(vl6180x_ptr->addr, 0x0018, 0x03);
	}
	
}

void start_measurement(VL6180X* vl6180x_ptr)
{
	i2c_range_write_byte(vl6180x_ptr->addr, 0x0018, 0x01);
}

// Update the sensor's distance measurement
// Returns 0 if the new and previous measurement were equal
uint8_t update_range(VL6180X* vl6180x_ptr)
{
	// Indicate read from RESULT_RANGE_VAL (0x62)
	i2c_transmit_buff[0] = 0x00;
	i2c_transmit_buff[1] = 0x62;
	i2c_receive_buff[0] = 0;
	i2c_write_buff(vl6180x_ptr->addr, 2);
	i2c_read_buff(vl6180x_ptr->addr, 1);

	uint8_t old_meas = vl6180x_ptr->distance;
	vl6180x_ptr->distance = i2c_receive_buff[0];

	return vl6180x_ptr->distance != old_meas;
}

// Error handler
void vl6180x_error_handler(void)
{
	while (1)
	{
	}
}