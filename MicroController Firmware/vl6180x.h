#ifndef VL6180X_H
#define VL6180X_H

#define RANGE_DEFAULT_SLAVE_ADDR 0x52

#include <stm32f4xx_hal.h>
#include "i2c.h"
#include "ov7690.h"

typedef struct
{
	uint8_t addr; // The slave address of the sensor
	uint8_t distance; // The most recent distance measurement
	GPIO_TypeDef* port; // The port that controls GPIO0 on the sensor
	uint16_t pin; // The pin that controls GPIO on the sensor

} VL6180X;

#define N_RANGE_DEVICES 2
VL6180X* range_devices[N_RANGE_DEVICES];

// Initialize sensor
void setup_range(VL6180X* vl6180x_ptr, uint8_t addr, GPIO_TypeDef* port, uint16_t pin);

// Change the sensor's slave address
uint8_t change_address(VL6180X* vl6180x_ptr, uint8_t new_addr);

// Begin ranging
void start_range(VL6180X* vl6180x_ptr);

// Update the sensor's distance measurement
// Returns 0 if the new and previous measurement were equal
uint8_t update_range(VL6180X* vl6180x_ptr);
void start_measurement(VL6180X* vl6180x_ptr);
uint8_t fix_address(VL6180X* vl6180x_ptr, uint8_t start_addr);
// Error handler
void vl6180x_error_handler(void);
#endif
